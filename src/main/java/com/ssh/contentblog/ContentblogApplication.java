package com.ssh.contentblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContentblogApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContentblogApplication.class, args);
	}
}

package com.ssh.contentblog.service;

import com.ssh.contentblog.exceptions.UserDoesNotExistException;
import com.ssh.contentblog.model.Post;
import com.ssh.contentblog.model.dto.PostDto;

import java.util.Optional;

public interface IPostService {
    void addPost(PostDto postDto) throws UserDoesNotExistException;

    Optional<Post> findPostById(Long id);
}

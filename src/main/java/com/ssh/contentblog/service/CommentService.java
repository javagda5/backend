package com.ssh.contentblog.service;

import com.ssh.contentblog.exceptions.PostDoesNotExistException;
import com.ssh.contentblog.exceptions.UserDoesNotExistException;
import com.ssh.contentblog.model.AppUser;
import com.ssh.contentblog.model.Comment;
import com.ssh.contentblog.model.Post;
import com.ssh.contentblog.model.dto.CommentDto;
import com.ssh.contentblog.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.Optional;

@Service
public class CommentService implements ICommentService {

    private CommentRepository commentRepository;
    private IPostService postService;
    private IAppUserService appUserService;

    @Autowired
    public CommentService(CommentRepository commentRepository, IPostService postService, IAppUserService appUserService) {
        this.commentRepository = commentRepository;
        this.postService = postService;
        this.appUserService = appUserService;
    }

    public void addComment(CommentDto commentDto) throws UserDoesNotExistException, PostDoesNotExistException {
        Optional<AppUser> appUserOptional = appUserService.getUserWithId(commentDto.getOwnerId());
        if (!appUserOptional.isPresent()) {
            throw new UserDoesNotExistException();
        }
        AppUser owner = appUserOptional.get();

        Optional<Post> postOptional = postService.findPostById(commentDto.getPostId());
        if (!appUserOptional.isPresent()) {
            throw new PostDoesNotExistException();
        }
        Post post = postOptional.get();

        Comment comment = new Comment(commentDto.getTitle(), commentDto.getContent(), owner, post);
        commentRepository.save(comment);
    }
}

package com.ssh.contentblog.service;

import com.ssh.contentblog.exceptions.RegistrationException;
import com.ssh.contentblog.exceptions.UserDoesNotExistException;
import com.ssh.contentblog.model.AppUser;
import com.ssh.contentblog.model.dto.LoginDto;
import com.ssh.contentblog.model.dto.responses.PageResponse;

import java.util.Optional;

public interface IAppUserService {
    void register(AppUser appUser) throws RegistrationException;

    PageResponse<AppUser> getUsers(int page);

    PageResponse<AppUser> getAllUsers();

    Optional<AppUser> getUserWithId(long ownerId);

    Optional<AppUser> getUserWithLoginAndPassword(LoginDto dto) throws UserDoesNotExistException;
}

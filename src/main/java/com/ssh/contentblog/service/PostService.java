package com.ssh.contentblog.service;

import com.ssh.contentblog.exceptions.UserDoesNotExistException;
import com.ssh.contentblog.model.AppUser;
import com.ssh.contentblog.model.Post;
import com.ssh.contentblog.model.dto.PostDto;
import com.ssh.contentblog.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PostService implements IPostService {
    private PostRepository postRepository;
    private IAppUserService appUserService;

    @Autowired
    public PostService(PostRepository postRepository, IAppUserService appUserService) {
        this.postRepository = postRepository;
        this.appUserService = appUserService;
    }

    @Override
    public void addPost(PostDto postDto) throws UserDoesNotExistException {
        Optional<AppUser> appUserOptional = appUserService.getUserWithId(postDto.getOwnerId());
        if (!appUserOptional.isPresent()) {
            throw new UserDoesNotExistException();
        }
        AppUser owner = appUserOptional.get();

        Post post = new Post(postDto.getTitle(), postDto.getContent(), owner);
        postRepository.save(post);
    }

    @Override
    public Optional<Post> findPostById(Long id){
        return postRepository.findById(id);
    }
}

package com.ssh.contentblog.component;

import com.ssh.contentblog.model.AppUser;
import com.ssh.contentblog.model.Role;
import com.ssh.contentblog.repository.AppUserRepository;
import com.ssh.contentblog.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer {

    private RoleRepository roleRepository;
    private AppUserRepository appUserRepository;

    @Autowired
    public DataInitializer(RoleRepository roleRepository, AppUserRepository appUserRepository) {
        this.roleRepository = roleRepository;
        this.appUserRepository = appUserRepository;
        loadData();
    }

    private void loadData() {
        Role adminRole = new Role("ADMIN");
        adminRole = roleRepository.saveAndFlush(adminRole);
        roleRepository.save(new Role("USER"));
        roleRepository.save(new Role("GUEST"));

        appUserRepository.save(new AppUser("admin", "admin", adminRole));
    }
}

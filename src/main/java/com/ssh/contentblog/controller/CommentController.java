package com.ssh.contentblog.controller;

import com.ssh.contentblog.exceptions.PostDoesNotExistException;
import com.ssh.contentblog.exceptions.UserDoesNotExistException;
import com.ssh.contentblog.model.dto.CommentDto;
import com.ssh.contentblog.model.dto.responses.RespFactory;
import com.ssh.contentblog.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/comment/")
public class CommentController {

    private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity add(@RequestBody CommentDto commentDto) {
        try {
            commentService.addComment(commentDto);
        } catch (UserDoesNotExistException | PostDoesNotExistException e) {
            return RespFactory.badRequest();
        }

        return RespFactory.created();
    }
}

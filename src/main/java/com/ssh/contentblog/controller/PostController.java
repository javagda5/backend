package com.ssh.contentblog.controller;

import com.ssh.contentblog.exceptions.UserDoesNotExistException;
import com.ssh.contentblog.model.dto.PostDto;
import com.ssh.contentblog.model.dto.responses.RespFactory;
import com.ssh.contentblog.service.IPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/post/")
public class PostController {

    private IPostService postService;

    @Autowired
    public PostController(IPostService postService) {
        this.postService = postService;
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity add(@RequestBody PostDto postDto){
        try {
            postService.addPost(postDto);
        } catch (UserDoesNotExistException e) {
            return RespFactory.badRequest();
        }

        return RespFactory.created();
    }
}

package com.ssh.contentblog.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String title;
    private String email;
    private String content;

    public Comment(String title, String content, AppUser sender, Post post) {
        this.dateTimeAdded = LocalDateTime.now();
        this.title = title;
        this.content = content;
        this.sender = sender;
        this.post = post;
    }

    private LocalDateTime dateTimeAdded;

    @OneToOne
    private AppUser sender;

    @OneToOne
    private Post post;

}

package com.ssh.contentblog.model.dto.responses;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@NoArgsConstructor
public class PageResponse<T> extends Response {
    private List<T> objects;
    private int currentPage;
    private int totalPages;

    public PageResponse(Page<T> objects) {
        this.currentPage = objects.getNumber();
        this.totalPages = objects.getTotalPages();

        this.objects = objects.getContent();
    }
}

package com.ssh.contentblog.model.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class MessageResponse extends Response {
    private String message;
}

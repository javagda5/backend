package com.ssh.contentblog.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentDto {
    private Long ownerId;
    private Long postId;

    private String title;
    private String email;
    private String content;
}

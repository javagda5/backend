package com.ssh.contentblog.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String title;
    private String content;
    private LocalDateTime dateTimeAdded;

    public Post(String title, String content, AppUser owner) {
        this.title = title;
        this.content = content;
        this.owner = owner;
        this.dateTimeAdded = LocalDateTime.now();
    }

    @Lob
    private byte[] image;

    @OneToOne
    private AppUser owner;

    @OneToMany
    private Set<Comment> commentSet;

}

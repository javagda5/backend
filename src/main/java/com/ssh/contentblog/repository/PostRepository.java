package com.ssh.contentblog.repository;

import com.ssh.contentblog.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {

}
